import { createSlice } from '@reduxjs/toolkit';
import { chunk  } from '../lib/index.js'; 



function __GetMovie( id , movies  ) {
    for( let index in movies ) {
        let movie = movies[ index ]; 
        if( movie.id === id )return { movie , index }; 
    }
    return { }; 
} 


export const moviesSlice = createSlice({
    name: "moviesApp",
    initialState: {
        movies: [],
        listing: [],
        filter: {
            categories: [],
            nbByPage: 12,
            page: 1,
            nbPages: 1
        },
        load: true,
        myfavorites: {}, 
        deleted: [], 
    },
    reducers: {
        
        dislike: (state, { payload } ) => {
            const id = payload;
            const { movie, index }  = __GetMovie( id , state.movies ); 
            if (typeof movie.id !== 'undefined') {
                if( typeof state.myfavorites[ id ] === 'undefined' ) {
                    state.myfavorites[ id ] = 1;
                } else {
                    if( state.myfavorites[ id ] === 1 ) {
                        movie.likes--;
                    } 
                }
                state.myfavorites[ id ]  = 0 ;
                movie.dislikes++; 
                state.movies[ index ] = movie; 
           } 
            
        },
        like: (state, { payload }  ) => { 
            const id = payload;
            const { movie, index }  = __GetMovie( id , state.movies ); 
           if (typeof movie.id !== 'undefined') {
                if( typeof state.myfavorites[ id ] === 'undefined' ) {
                    state.myfavorites[ id ] = 1;
                } else {
                    if( state.myfavorites[ id ] === 0 ) {
                        movie.dislikes--;
                    } 
                }
                state.myfavorites[ id ]  = 1 ;
                movie.likes++; 
                state.movies[ index ] = movie; 
           } 
            
        },
        listing: (state, { payload = { } }) => {
            let { filter = { }, debug = false  } =  payload;
            filter = { ...state.filter , ...filter };
            const { nbByPage, categories, page = 1   } = filter; 
            if ( debug  ) console.log( "bbb" , state.filter.nbByPage ); 
            let listing = []; 
            for( let moviesrc of state.movies ) {
                const movie = Object.assign( { } , moviesrc );
                if( categories.length > 0 ) {
                    if ( categories.indexOf( movie.category ) !== -1 ) {
                       listing.push( movie );
                    }
                } else {
                    listing.push( movie );
                }
            } 
            state.filter = filter; 
            if (listing.length > 0 ) {
                listing = chunk( listing, nbByPage );
                state.filter.nbPages = listing.length; 
                if( typeof  listing[ page - 1 ] === 'undefined' ) {
                    state.listing = []; 
                } else {
                    state.listing = listing[ page - 1 ]; 
                }
               
            }

        
        
           
            
        },
        loaded: (state) => {
            state.load = false; 
        },
        navigate: ( state, { payload } )=> {
            const direction = payload ;
            if ( state.listing.length < 2) return state;
            let page = state.filter.page;
            if( direction === 'back') {
                if ( page === 1 ) return state;
                page = ( page - 1 );
            }  else {
                if( state.filter.nbPages < ( page + 1 ) ) return state;
                page = ( page + 1 );
            }
           
            state.filter.page = page; 
            
        
        },
        remove: (state, { payload } ) => {
            const id = payload;
            if ( state.deleted.indexOf( id ) !== -1 ) return false; 
            var tmp = [];
            for( let movie of state.movies ) {
                if( movie.id !== id ) {
                    tmp.push( movie );
                } 
            }
            state.deleted.push( id ); 
            state.movies = tmp;
        },
        setList: ( state, { payload } ) => {
            state.movies = payload ;
        }
       
        
     }
});

export const { like, dislike , remove, setList ,  loaded , navigate,  listing   } = moviesSlice.actions;



export default moviesSlice.reducer; 
