

import { configureStore } from '@reduxjs/toolkit';
import moviesReducer from './moviesReducer.js';

const initialState = {
   movies_list: []
}

const store =  configureStore({
  reducer: {
    moviesApp: moviesReducer,
    initialState
  },
})

export { store }; 