import React from 'react';
import '../../styles/components/Cards.css';
import {useDispatch } from 'react-redux';
import { remove, like, dislike, listing  } from '../redux/moviesReducer.js'; 
import { LikeButton  } from './LikeButton.js';






export function Cards ({movie}) {
    const dispatch = useDispatch(); 
    const { title , id  , category, likes, dislikes  } = movie;
    var key = 'card-' + id; 
    const RemoveCard = (id) => {
        dispatch( remove(id)); 
        dispatch( listing() );
    }
    const LikeToggleHandle = (status  ) => {
        if( status === -1 ) return false; 
        if( status === 1 ) {
            dispatch( like( id ));
        } else {
            dispatch( dislike( id ));
        }
        dispatch( listing() );
    }
   

    return (<div className="Card-Movie" key={key} >
        <div className="Card-Content" >
            <div className="Card-handle" >
                <div className="Movie-Title" >
                    {title}
                </div>
                <button className="Circle-Button c-red" onClick={() => RemoveCard(id) }  >
                    <i className="fas fa-trash-alt"></i>
                </button>
            </div>
            <div className="Movie-Category">
                {category}
            </div>
            <div className="Move-Like-Dislike" >
                <div className="Movie-Stats" >
                    <div ><i className="far fa-thumbs-up"></i>:  {likes}</div>
                    <div ><i className="far fa-thumbs-down"></i>:  {dislikes}</div>
                </div>
                <div className="LikeButton-Container" >
                    <LikeButton id={id} onChange={(status) => LikeToggleHandle(status) }/>
                </div>
            </div>
        </div>
        
        

    </div>)
}

