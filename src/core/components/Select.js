import React from 'react';
import './../../styles/components/Select.css';


export class Select extends React.Component {
    static defaultProps = {
        label: '',
        values: [], 
        placeHolder: '',
        list: [],
        multiple: false,
        onChange: () => { },
        classes: ""
    }
    constructor( props ) {
        super( props );
        this.state ={
            opened: false,
            values: this.props.values
         };
    }
    render() {
        const { opened } = this.state; 
        const { placeHolder, classes  } = this.props; 
        const open = () => this.openToggle();
        let text = this.GetSelectedText(); 
        if( text === '') {
            text = <span className="Select-PlaceHolder">{placeHolder}</span>; 
        }
    

        return (<div className={"Select-Container " + classes } >
           <div className="Select-Content" >
            <div className="Select-Handle" onClick={open} >
                    <label className="Select-Label" >
                        {this.props.label}: 
                    </label>
                    <div className="Select-Text" >
                        {text}
                    </div>
                    <div className="Select-Arrow-Button" >
                        <i className={ "fas fa-" + ( opened ? 'chevron-up' : 'chevron-down') } ></i>
                    </div>
                </div>
                {this.renderList()}
           </div>
        </div>)
    }

    renderList() {
        if ( !this.state.opened ) return false;
        const { list } = this.props;
        let tmp = [];
        if( Array.isArray( list )) {
            for( let i in list ) {
                const value = list[ i ];
                const key =  "select-choice-" + i ;  
                const selected = ( this.state.values.indexOf( value ) !== -1 ? <i className="far fa-check-circle"></i> : false ); 
                tmp.push( 
                    <div className="Select-Choice" key={key} onClick={() => this.Choose( i, value )} >
                        <span>{value}</span>
                        {selected}
                    </div>
                );
            }
        } else {
            for( let i in list ) {
                const { label, value } = list[ i ];
                const key =  "select-choice-" + i ;  
                const selected = ( this.state.values.indexOf( value ) !== -1 ? <i className="far fa-check-circle"></i> : false ); 
                tmp.push( 
                    <div className="Select-Choice" key={key} onClick={() => this.Choose( i, value )} >
                        {label}
                        {selected}
                    </div>
                );
            }
        }
        
        return (<div className="Select-List" >
            {tmp}
        </div>)
    }

    /**
     * Get Selected text
     * @returns {string}
     */
    GetSelectedText() {
        const { values } = this.state; 

        const { list } = this.props; 
        let text = [];
        if( Array.isArray( list )) {
            for( let value of list ) { 
                if( values.indexOf( value ) !== -1)  text.push( value );
            }
        } else {
            for( let data of list ) {
                const { label , value } = data;
                console.log( values.indexOf( value ) ); 
                if( values.indexOf( value ) !== -1) text.push( label ); 
            }
        }
        return text.join(',');
    }

    async openToggle() {
        let opened = this.state.opened;
        this.setState({
            opened: (opened ? false:  true )
        });
    }

    async Choose( index, value ) {
        const { multiple } = this.props;
        let values = Object.assign([], this.state.values );
        if( multiple ) {
            console.log( values ); 
            if ( values.indexOf( value ) === -1  ) {
                values.push( value ); 
            } else {
                values.splice(  values.indexOf( value  ), 1 ); 
            }
        } else {
            values = [ value ];
        }
        await this.setState({
            values
        });
        if( !multiple ) this.openToggle(); 
        this.props.onChange( this.state.values ); 
    }
}