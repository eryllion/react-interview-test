import React from 'react';
import '../../styles/MoviesView.css';
import { Cards } from './Cards.js'; 

export class MovieListing extends React.Component {
    static defaulProps = {
        movies: []
    }
    constructor(props) {
        super(props);
        this.state = { }; 
    }
    render() {
        const { movies } = this.props; 
        if( movies.length === 0 ) return (<div id="MoviesListing" style={{ justifyContent:  'center' }} ><h2>Aucun film trouvé</h2></div>); 
        

        return (
        <div id="MoviesListing" >
            {movies.map( movie => <Cards movie={movie} key={ 'card-' + movie.id  }/> )}
        </div>); 
    }
    
}