import React from 'react';
import '../../styles/components/LikeButton.css' ;


export class LikeButton extends React.Component  {
    static defaultProps = {
        onChange: () => {},
        id: ''
    }
    constructor( props ) {
        super( props );
        this.state = { 
            // status {Number} -1 : not defined (default)  |  0: dislike  |  1: like
            status: -1

        }
    }
    render() {
        let classes = ["Circle-Button", "LikeButton", "box-center"];
        let icon = <i className="far fa-heart"></i>;
        let text = ""; 
        if( this.state.status === 1 ) {
            classes.push("Liked"); 
            icon = <i className="fas fa-heart"></i>;
            text = "Vous aimez"; 
        } else if ( this.state.status === 0  ) {
            classes.push("DisLiked"); 
            icon = <span className="UnLicked-Icon ">
                        <i className="far fa-heart fa-stack-1x"></i>
                        <i className="fas fa-ban fa-stack-2x c-black" ></i>
                    </span>; 
            text = "Vous n'aimez pas";         
        } else { }

        return (
        <div className="LikeButton-Container column" >
            <button className={classes.join(' ')} onClick={() => this.likeToggle() } >
                 {icon}
            </button>
            <div className="LikeButton-text" >
                {text}
            </div>
        </div>
        
        )
    }

    async likeToggle () {
        let status = this.state.status;
        await this.setState({ 
            status: ( status === 1 ? 0 : 1)
        });
        this.props.onChange( this.state.status )
        
    }
}
