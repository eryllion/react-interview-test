import React from 'react';
import '../../styles/components/Pagination.css';
import { useSelector, useDispatch } from 'react-redux';
import { navigate , listing } from "../redux/moviesReducer.js";

export function Pagination ( ) {
    const dispatch = useDispatch();
    const state = useSelector( state => state.moviesApp );
    const Nav = async (direction) => {
        await dispatch( navigate( direction  ));
        dispatch( listing(  )); 
    }
    const { page } = state.filter; 
    return (<div className="Pagination-Container" >
            <button className="Pagination-Nav left-nav" onClick={() => { 
                Nav('back'); 
            }} >
                <i className="fas fa-chevron-left"></i>
            </button> 
            <div className="flexOne box-center" >
                Page {page} | {state.listing.length} résultat(s)
            </div>
            <button className="Pagination-Nav right-nav" onClick={() => { 
                Nav('next'); 
            }} >
                <i className="fas fa-chevron-right"></i>
            </button> 
        </div>)
}

/*

export class Pagination extends React.Component {
    static defaultProps = {
        page: 1,
        nb: 0,
        total: 0,
        onChange: () => {  }
    }
    constructor(props) {
        super( props );
        this.state = {

        };
    }
    render() {

        return (<div className="Pagination-Container" >
            <button className="Pagination-Nav left-nav" onClick={() => { 
                this.props.onChange('back'); 
            }} >
                <i className="fas fa-chevron-left"></i>
            </button> 
            <div className="flexOne box-center" >
                Page 1 ( 4 / 12 )
            </div>
            <button className="Pagination-Nav right-nav" onClick={() => { 
                this.props.onChange('next'); 
            }} >
                <i className="fas fa-chevron-right"></i>
            </button> 
        </div>)
    }
    
}**/