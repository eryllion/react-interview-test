import React from 'react';
import '../../styles/MoviesView.css'; 
import { MovieListing,   Pagination, Select } from '../components/index.js'; 
import { movies$ } from '../movies/movies.js'; 
import { useSelector, useDispatch } from 'react-redux';
import { setList, loaded, listing} from "../redux/moviesReducer.js";

export function Listing(state) {
    const { load , listing = [ ] } = state;
    if( load ) return ( <div className="Loader fa-3x" ><i className="fas fa-spinner fa-spin"></i></div>); 
    return <MovieListing movies={listing} />
}


export function MoviesView () {
    const dispatch = useDispatch();
    const selector = useSelector( state => state.moviesApp  );

    /** load Movies async  */
    async function LoadMovies() {
        const movies = await movies$;
       
        dispatch(  setList( movies ) ); 
        dispatch( loaded() ); 
        dispatch( listing({ filter: { }})) ; 
    }
    if( selector.load ) LoadMovies( );

    const GetCategories = () => {
        let categories = [];
        for( let i in  selector.movies ) {
            if( categories.indexOf( selector.movies[i].category) === -1)  categories.push( selector.movies[i].category ); 
        }
        return categories; 
    }

    const Search = ( filter  ) => {
        dispatch( listing( { filter } )) ; 
    }
   

    

    return (<div id="MoviesView" className="flexOne column"  >
            <div id="header" className="box-center" >
                <h1>MOVIES</h1>
            </div>
            <div id="filter-tab"  className="row" >
                <div className="flexOne filter">
                    <Select 
                        label="Filtrer" 
                        values={[]} 
                        list={GetCategories()}
                        
                        placeHolder="Cliquer ici pour filtrer"
                        multiple={true}
                        onChange={(values) => Search( { categories: values, page: 1 })}  
                    />
                </div> 
                <div className="flexOne filter">
                    <Select 
                        label={ <span><i className="fas fa-eye" ></i> Afficher </span> }
                        values={[ 12 ]} 
                        list={[ 4, 8 , 12, 16]}
                        onChange={(values) => Search( { nbByPage: values[0], page: 1 }) }
                        classes="right"
                    />
                </div> 
            </div>
            <Pagination />
            {Listing( selector )}
           

        </div>)
}

