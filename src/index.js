import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';

//import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { store  } from './core/redux/index.js';
import { MoviesView } from './core/views/index.js'; 


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store} >
        <MoviesView />
    </Provider>
   
  </React.StrictMode>,
  document.getElementById('root')
);

